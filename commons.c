//
// Created by notnaturalselection on 11.12.2021.
//

#include <malloc.h>
#include "commons.h"

bool **alloc_2d_field(uint16_t rows, uint16_t columns) {
    bool **result = calloc(rows, sizeof(bool *));
    for (int i = 0; i < rows; ++i) {
        result[i] = calloc(columns, sizeof(bool));
    }
    return result;
}

void free_2d_field(bool **ptr, uint16_t rows) {
    for (int i = 0; i < rows; ++i) {
        free(ptr[i]);
    }
    free(ptr);
}