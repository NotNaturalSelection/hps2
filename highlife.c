//
// Created by notnaturalselection on 11.12.2021.
//

#include <bits/types/FILE.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "highlife.h"
#include "stdlib.h"
#include "commons.h"

#define NEW_LINE '\n'
#define BUF_SIZE 4096

void print_raw(bool is_vertical_by_default, uint16_t src_row_size, uint16_t src_column_size, bool **data, FILE *out, uint16_t row_offset) {
    char buf[BUF_SIZE] = {0};
    size_t number = 0;
    uint32_t row_num = is_vertical_by_default ? src_row_size : src_column_size;
    uint32_t column_num = is_vertical_by_default ? src_column_size : src_row_size;
    for (int i = 0; i < row_offset; ++i) {
        buf[number++] = NEW_LINE;
    }
    for (int i = 0; i < row_num; ++i) {
        for (int j = 0; j < column_num; ++j) {
            if (is_vertical_by_default) {
                buf[number++] = data[i][j] ? '1' : '0';
            } else {
                buf[number++] = data[j][i] ? '1' : '0';
            }
        }
        buf[number++] = NEW_LINE;
    }
    fputs(buf, out);
}

void print_field(highlife_field *field, FILE *out) {
    print_raw(field->is_vertical_by_default, field->rows, field->columns, field->data, out, 0);
}

bool *parse_str(char *input, size_t *size) {
    *size = strlen(input);
    bool *result = calloc(*size, sizeof(bool));
    for (int i = 0; i < *size; ++i) {
        result[i] = input[i] - '0';
    }
    return result;
}

void transpose(highlife_field *field) {
    bool **new_data = alloc_2d_field(field->columns, field->rows);
    for (int i = 0; i < field->rows; ++i) {
        for (int j = 0; j < field->columns; ++j) {
            new_data[j][i] = field->data[i][j];
        }
    }
    free_2d_field(field->data, field->rows);

    field->data = new_data;
    uint32_t tmp = field->columns;
    field->columns = field->rows;
    field->rows = tmp;
}

highlife_field *read_field(char *path) {
    FILE *input = fopen(path, "r");
    if (input == NULL) {
        return NULL;
    }

    char buf[BUF_SIZE];
    fread(buf, sizeof(char), BUF_SIZE, input);
    fclose(input);
    char *token = strtok(buf, "\n");
    if (token == NULL) {
        return NULL;
    }

    bool *ptr_buf[BUF_SIZE];
    size_t line_count = 0;
    size_t line_size = 0;
    while (token != NULL) {
        ptr_buf[line_count++] = parse_str(token, &line_size);
        token = strtok(NULL, "\n");
    }
    bool **data = calloc(line_count, sizeof(bool *));
    memcpy(data, ptr_buf, line_count * sizeof(bool *));
    highlife_field *result = calloc(1, sizeof(highlife_field));
    result->filepath = path;
    result->data = data;
    result->rows = line_count;
    result->columns = line_size;
    result->is_vertical_by_default = line_count >= line_size;
    if (!result->is_vertical_by_default) {
        transpose(result);
    }
    result->cycle = 0;
    return result;
}

void divide_by_parts(uint16_t *arr, int size, uint32_t dividend) {
    uint32_t total_rows = dividend;
    uint16_t mean_part_size = floor(((double) dividend) / ((double) size));
    for (int i = 0; i < size; ++i) {
        arr[i] = mean_part_size;
        total_rows -= mean_part_size;
    }
    for (int i = 0; i < size; ++i) {
        if (total_rows > 0) {
            arr[i] += 1;
            total_rows--;
        }
    }
}

void get_field_part(highlife_field *field, const uint16_t row_size_res, const uint16_t start_row_num_res, bool **arr) {
    for (int i = 0; i < row_size_res; ++i) {
        memcpy(arr[i], field->data[start_row_num_res + i], sizeof(bool) * field->columns);
    }
}


int get_field_part_coords(highlife_field *field, int part_num, int total_parts, uint16_t *start_row_num,
                          uint16_t *row_size) {
    if (part_num > total_parts) {
        return -1;
    }
    uint16_t sizes[total_parts];
    if (total_parts > field->rows) {
        for (int i = 0; i < field->rows; ++i) {
            sizes[i] = 1;
        }
    } else {
        divide_by_parts(sizes, total_parts, field->rows);
    }
    for (int i = 0; i < part_num; ++i) {
        *start_row_num = *start_row_num + sizes[i];
    }
    *row_size = sizes[part_num];
    return 0;
}

int count_living(bool **bordered_arr, uint16_t i, uint16_t j) {
    int living = 0;
    for (int k = -1; k < 2; ++k) {
        for (int l = -1; l < 2; ++l) {
            if (k != 0 || l != 0) {
                if (bordered_arr[i + k][j + l]) {
                    living++;
                }
            }
        }
    }
    return living;
}

bool **create_bordered_field(bool **src_arr, uint16_t rows, uint16_t columns) {
    bool **result = alloc_2d_field(rows + 2, columns + 2);
    for (int i = 0; i < rows; ++i) {
        memcpy(result[i + 1] + sizeof(bool), src_arr[i], columns * sizeof(bool));
    }
    return result;
}

bool **live_cycle(bool **bordered_arr, uint16_t rows, uint16_t columns) {
    bool **result = alloc_2d_field(rows, columns);
    for (int i = 0; i < rows - 2; ++i) {
        for (int j = 0; j < columns - 2; ++j) {
            int living = count_living(bordered_arr, i + 1, j + 1);
            if (bordered_arr[i + 1][j + 1]) {
                if (living != 2 && living != 3) {
                    result[i][j] = false;
                } else {
                    result[i][j] = true;
                }
            } else {
                if (living == 3 || living == 6) {
                    result[i][j] = true;
                } else {
                    result[i][j] = false;
                }
            }
        }
    }
    return result;
}