//
// Created by notnaturalselection on 11.12.2021.
//

#ifndef HPS2_HIGHLIFE_H
#define HPS2_HIGHLIFE_H

#include <stdbool.h>
#include <stdint-gcc.h>
#include "stdio.h"

typedef struct {
    char *filepath;
    uint32_t cycle;
    uint32_t columns;
    uint32_t rows;
    bool is_vertical_by_default;
    bool **data;
} highlife_field;

void print_field(highlife_field *field, FILE*out);

highlife_field *read_field(char *path);

bool** live_cycle(bool**bordered_arr, uint16_t rows, uint16_t columns);

void get_field_part(highlife_field *field, uint16_t row_size_res, uint16_t start_row_num_res, bool **arr);

int get_field_part_coords(highlife_field *field, int part_num, int total_parts, uint16_t *start_row_num,
                          uint16_t *row_size);

void print_raw(bool is_vertical_by_default, uint16_t src_row_size, uint16_t src_column_size, bool **data, FILE*out, uint16_t row_offset);

bool **create_bordered_field(bool **src_arr, uint16_t rows, uint16_t columns);

#endif //HPS2_HIGHLIFE_H
