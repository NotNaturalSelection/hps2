//
// Created by notnaturalselection on 12.12.2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "highlife.h"
#include "commons.h"

void log_state(bool **data, char *dest_dir, uint16_t cycle_num, bool is_vertical_by_default, uint16_t rows,
               uint16_t columns) {
    char filename[300] = {0};
    sprintf(filename, "%s/day_%d_total_result.txt", dest_dir, cycle_num);
    FILE *out = fopen(filename, "w+");
    print_raw(is_vertical_by_default, rows, columns, data, out, 0);
    fclose(out);
}

int main(int argc, char **argv) {
    if (argc < 4) {
        puts("Not enough arguments. Usage: <src_file_path> <cycle_count> <dest_dir_path>");
        return EXIT_FAILURE;
    }
    uint32_t cycles = strtol(argv[2], NULL, 10);
    highlife_field *field = read_field(argv[1]);
    if (field == NULL) {
        fputs("Maximum field length exceeded or file not found", stderr);
        return EXIT_FAILURE;
    }
    for (int cycle_num = 0; cycle_num < cycles; ++cycle_num) {
        bool **bordered = create_bordered_field(field->data, field->rows, field->columns);
        memcpy(bordered[0] + sizeof(bool), bordered[field->rows] + sizeof(bool), field->columns);
        memcpy(bordered[field->rows + 1] + sizeof(bool), bordered[1] + sizeof(bool), field->columns);
        for (int i = 1; i < field->rows + 1; ++i) {
            bordered[i][0] = bordered[i][field->columns];
        }
        for (int i = 1; i < field->rows + 1; ++i) {
            bordered[i][field->columns + 1] = bordered[i][1];
        }
        bordered[0][0] = bordered[field->rows][field->columns];
        bordered[0][field->columns + 1] = bordered[field->rows][1];
        bordered[field->rows + 1][0] = bordered[1][field->columns];
        bordered[field->rows + 1][field->columns + 1] = bordered[1][1];
        bool **next_step = live_cycle(bordered, field->rows + 2, field->columns + 2);
        log_state(next_step, argv[3], cycle_num + 1, field->is_vertical_by_default, field->rows, field->columns);
        free_2d_field(bordered, field->rows + 2);
        free_2d_field(field->data, field->rows);
        field->data = next_step;
    }
    return EXIT_SUCCESS;
}