//
// Created by notnaturalselection on 11.12.2021.
//

#ifndef HPS2_MPI_UTILS_H
#define HPS2_MPI_UTILS_H

#include "stdio.h"
#include "highlife.h"

void log_from_0_pid(char *msg, int rank, FILE *dst);

void send_result(bool **result, uint16_t rows, uint16_t columns);

void receive_result(int rank, uint16_t rows, highlife_field*field, uint16_t start_row);

void send_part_async(bool *row, uint16_t columns, int dest_rank, bool is_sender_above);

void receive_part(bool *buf, uint16_t columns, int src_rank, bool is_sender_above);

#endif //HPS2_MPI_UTILS_H
