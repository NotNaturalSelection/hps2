//
// Created by notnaturalselection on 11.12.2021.
//

#ifndef HPS2_COMMONS_H
#define HPS2_COMMONS_H

#include <stdbool.h>
#include <stdint-gcc.h>

bool **alloc_2d_field(uint16_t rows, uint16_t columns);

void free_2d_field(bool **ptr, uint16_t rows);
#endif //HPS2_COMMONS_H
