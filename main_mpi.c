#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi/mpi.h"
#include "highlife.h"
#include "mpi_utils.h"
#include "commons.h"

#define PID_TOTAL (-1)

void log_state(bool **data, char *dest_dir, uint16_t cycle_num, bool is_vertical_by_default, uint16_t rows,
               uint16_t columns, int pid, uint16_t row_offset) {
    char filename[300] = {0};
    if (pid == PID_TOTAL) {
        sprintf(filename, "%s/day_%d_total_result.txt", dest_dir, cycle_num);
    } else {
        sprintf(filename, "%s/day_%d_pid_%d_result.txt", dest_dir, cycle_num, pid);
    }
    FILE *out = fopen(filename, "w+");
    print_raw(is_vertical_by_default, rows, columns, data, out, row_offset);
    fclose(out);
}

void fill_borders(bool **bordered_part, uint16_t row_num, uint16_t row_len, bool *row_above,
                  bool *row_below, int rank) {
    memcpy(bordered_part[0] + sizeof(bool), row_above, row_len);
    memcpy(bordered_part[row_num + 1] + sizeof(bool), row_below, row_len);
    for (int i = 1; i < row_num + 1; ++i) {
        bordered_part[i][0] = bordered_part[i][row_len];
    }
    for (int i = 1; i < row_num + 1; ++i) {
        bordered_part[i][row_len + 1] = bordered_part[i][1];
    }
    bordered_part[0][0] = row_above[row_len - 1];
    bordered_part[0][row_len + 1] = row_above[0];
    bordered_part[row_num + 1][0] = row_below[row_len - 1];
    bordered_part[row_num + 1][row_len + 1] = row_below[0];
}

bool **
live_cycle_of_part(highlife_field *field, uint16_t rows, uint16_t start_row, int rank, char *dest_dir,
                   bool *row_above, bool *row_below, int cycle, bool **part_field) {
    bool **bordered_part = create_bordered_field(part_field, rows, field->columns);

    fill_borders(bordered_part, rows, field->columns, row_above, row_below, rank);

    bool **next_part = live_cycle(bordered_part, rows + 2, field->columns + 2);
    log_state(next_part, dest_dir, cycle, field->is_vertical_by_default, rows, field->columns, rank,
              start_row);
    free_2d_field(part_field, rows);
    free_2d_field(bordered_part, rows + 2);
    return next_part;
}

void
collect_result(int rank, int size, highlife_field *field, char *dest_dir, bool **next_part, uint16_t rows, int cycle) {
    if (rank == 0) {
        for (int i = 0; i < rows; ++i) {
            memcpy(field->data[i], next_part[i], field->columns * sizeof(bool));
        }
        for (int i = 1; i < size; ++i) {
            uint16_t sender_start_row = 0;
            uint16_t sender_row_size = 0;
            get_field_part_coords(field, i, size, &sender_start_row, &sender_row_size);
            receive_result(i, sender_row_size, field, sender_start_row);
        }
        log_state(field->data, dest_dir, cycle, field->is_vertical_by_default, field->rows, field->columns, PID_TOTAL,
                  0);
    } else {
        send_result(next_part, rows, field->columns);
    }
}

int main(int argc, char **argv) {
    int rank, pids;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &pids);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (argc < 4) {
        log_from_0_pid("Not enough arguments\nUsage: <src_file_path> <cycle_count> <dest_dir_path>", rank, stderr);
        return EXIT_FAILURE;
    }
    uint32_t cycles = strtol(argv[2], NULL, 10);
    highlife_field *field = read_field(argv[1]);
    if (field == NULL) {
        log_from_0_pid("Maximum field length exceeded or file not found", rank, stderr);
        return EXIT_FAILURE;
    }
    uint16_t start_row_num_res = 0;
    uint16_t row_size_res = 0;
    int res = get_field_part_coords(field, rank, pids, &start_row_num_res, &row_size_res);
    if (res == -1) {
        puts("Field is unsplittable");
        return EXIT_FAILURE;
    }

    bool **part_field = alloc_2d_field(row_size_res, field->columns);
    get_field_part(field, row_size_res, start_row_num_res, part_field);
    bool **next_part = live_cycle_of_part(field, row_size_res, start_row_num_res, rank, argv[3],
                                          field->data[(start_row_num_res + (field->rows - 1)) % field->rows],
                                          field->data[(start_row_num_res + row_size_res) % field->rows], 1, part_field);

    collect_result(rank, pids, field, argv[3], next_part, row_size_res, 1);
//    MPI_Barrier(MPI_COMM_WORLD);
    for (int i = 2; i <= cycles; ++i) {
        uint16_t rank_above = (rank + pids - 1) % pids;
        uint16_t rank_below = (rank + 1) % pids;
//        printf("%d: above:%d below:%d\n", rank, rank_above, rank_below);
//        printf("sending part from %d to %d, cycle %d\n", rank,rank_above, i);
        send_part_async(next_part[0], field->columns, rank_above, true);
//        printf("sending part from %d to %d, cycle %d\n", rank,rank_below, i);
        send_part_async(next_part[row_size_res - 1], field->columns, rank_below, false);
//        MPI_Barrier(MPI_COMM_WORLD);
        bool row_above[field->columns];
        bool row_below[field->columns];
//        printf("%d receiving part from %d, cycle %d\n", rank, rank_below, i);
        receive_part(row_below, field->columns, rank_below, true);
//        printf("%d receiving part from %d, cycle %d\n", rank, rank_above, i);
        receive_part(row_above, field->columns, rank_above, false);
//        printf("all parts received by %d, cycle %d\n", rank, i);

        next_part = live_cycle_of_part(field, row_size_res, start_row_num_res, rank, argv[3],
                                       row_above, row_below, i, next_part);
//        MPI_Barrier(MPI_COMM_WORLD);
        collect_result(rank, pids, field, argv[3], next_part, row_size_res, i);
    }


    MPI_Finalize();
    return EXIT_SUCCESS;
}